#!/bin/bash

for scriptdir in $(find -L /opt/infrastructure -type d -name scripts 2>/dev/null); do
   PATH=$PATH:$scriptdir
done
export PATH
