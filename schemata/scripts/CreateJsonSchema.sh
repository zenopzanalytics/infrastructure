#!/bin/bash

if [ -z $1 ]; then
   echo "Must pass in a file name.  Exiting."
   exit
fi

if [ -z $2 ]; then
   echo -n "Schema name: "
   read name
else
   name=$2
fi

indent() {
   let spaces=($1+1)*3
   #seq -f "." -s "" ${spaces}
   seq -f " " -s "" ${spaces}
}

output() {
   echo "$(indent ${level})$1"
}

input=$1
let level=0
comma=false
id="http://zenopzanalytics.com/schemata/json/${name}#"
schemaref="http://json-schema.org/draft-04/schema#"


output "{"
let level=${level}+1
output "\"id\": \"${id}\","
output "\""'$schema'"\": \"${schemaref}\","
output "\"type\": \"object\","
output "\"definitions\": {},"
output "\"additionalProperties\": false,"
output "\"properties\":"
let baselevel=${level}
while read input; do
   parts=(${input})
   let nump=${#parts[@]}
   if [[ ${nump} -gt 1 ]]; then
      let lastp=${nump}-1
      b="${parts[${lastp}]}"
      allp="${parts[@]}"
      a="${allp% ${b}}"
   else
      a="${parts[@]}"
      b=""
   fi
   #echo "A:###${a}### B:###${b}###"
   if [[ "${a}" = "" ]]; then
      echo "Do nothing" > /dev/null
   elif [[ "${b}" = "" ]]; then
      if [[ "${a}" = "}" || "${a}" = "}," ]]; then
         let level=${level}-1
         if [[ ${level} -gt ${baselevel} ]]; then
            output "}"
            let level=${level}-1
            output "${a}"
         else
            output "${a},"
         fi
      elif [[ "${a}" = "{" ]]; then
         output "${a}"
         let level=${level}+1
      elif [[ "${a}" = "]" || "${a}" = "]," ]]; then
         a1=$(echo "${a}" | tr ']' '}')
         let level=${level}-1
         output "${a1}" 
      else
         a1=$(echo "${a}" | cut -d'"' -f2)
         case "${a1}" in
            string) a2="${a1}";;
            number) a2="long";;
         esac
         output "\"type\": \"${a1}\","
         output "\"title\": \"TBD\","
         output "\"default\": \"\""
         let level=${level}-1
         output "}"
      fi
   elif [[ "${b}" = "{" ]]; then
      output "${a} {"
      let level=${level}+1
      output "\"type\": \"object\","
      output "\"properties\": {"
      let level=${level}+1
   elif [[ "${b}" = "[" ]]; then
      output "${a} {"
      let level=${level}+1
      output "\"type\": \"array\","
      output "\"items\": {"
      let level=${level}+1
   else
      b1=$(echo "${b}" | cut -d'"' -f2)
      b2=$(echo "${b}" | cut -d'"' -f3)
      case "${b1}" in
         string) b3="${b1}";;
         number) b3="long";;
      esac
      if [[ "${b2}" = "," ]]; then
         comma=true
      fi
      output "${a} {"
      let level=${level}+1
      output "\"type\": \"${b3}\","
      output "\"title\": \"TBD\","
      output "\"default\": \"\""
      let level=${level}-1
      if ( ${comma} ); then
         output "},"
         comma=false
      else
         output "}"
      fi
   fi
done <${input}
output "\"required\": []"
let level=${level}-1
output "}"
