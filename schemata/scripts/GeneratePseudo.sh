#!/bin/bash

if [ -z $1 ]; then
   schema_name="mac_general"
else
   schema_name="$1"
fi

sample_file=${schema_name}/sample.raw
schema_file=${schema_name}/schema
rm -rf ${schema_file}.*
temp_schema_file="/tmp/${schema_name}"

if [ -d ${schema_name} ]; then
   if [ ! -r ${sample_file} ]; then
      cat $(find ${schema_name} -type f -name "sdc-*") > ${sample_file}
   fi
else
   echo "Directory ${schema_name} doesn't exist.  Exiting."
   exit
fi

explore() {
   def="${def}{ "
   local first="Y"
   local input=$1
   OIFS=$IFS && IFS=$'\n' 
   local kms=($(echo "${input}" | jq -c 'to_entries[] | {key,type:(.value | type)}'))
   IFS=$OIFS
   local i=0
   while [[ $i -lt ${#kms[@]} ]]; do
      local km="${kms[$i]}"
      local key=$(echo "${km}" | jq '.key')
      local type=$(echo "${km}" | jq '.type')
      if [[ "${first}" = "Y" ]]; then
         def="${def}${key}: "
         first="N"
      else
         def="${def},${key}: "
      fi
      local value=$(echo "${input}" | jq --arg key "${key}" ".[$key]")
      if [[ "${type}" = "\"object\"" ]]; then
         explore "${value}"
      elif [[ "${type}" = "\"array\"" ]]; then
         local j=0
         local len=$(echo "${input}" | jq --arg key "${key}" ".[$key] | length")
         local subtypes
         local subtype
         while [[ $j -lt ${len} ]]; do
            subtype=$(echo "${input}" | jq --arg key "${key}" ".[$key][$j] | type")
            subtypes="${subtypes} ${subtype}"
            let j=$j+1
         done
         local types=($(echo "${subtypes}" | tr ' ' '\n' | sort -u))
         let j=0
         local atypes
         while [[ $j -lt ${#types[@]} ]]; do
            if [[ $j -eq 0 ]]; then
               atypes="${types[$j]}"
            else
               atypes="${atypes},${types[$j]}"
            fi
            let j=$j+1
         done
         def="${def}[${atypes}] "
      else
         def="${def}${type} "
      fi
      let i=$i+1
   done
   def="${def}}"
}

let i=1
let v=1
echo -n "Reading record "
while read -r sample; do
   let n=0
   recs=$(echo "${sample}" | jq -s 'length')
   if [[ ${recs} -gt 1 ]]; then
      echo -n "Line has ${recs} records."
   fi
   while [[ $n -lt ${recs} ]]; do
      record=$(echo "${sample}" | jq -s --arg num "${n}" ".[$n]")
      echo -n "$i."
      def=""
      ts=$(echo "${record}" | jq '.["@timestamp"]')
      explore "${record}"
      if [ -f ${schema_file}.1 ]; then
         #echo "${def}" | jq '.' > ${temp_schema_file}
         echo "${def}" | jq -S '.' > ${temp_schema_file}
         let j=1
         match="N"
         while [[ $j -lt $v ]]; do
            diff_result=$(diff ${schema_file}.$j ${temp_schema_file})
            if [[ "${diff_result}" = "" ]]; then
               match="Y"
               echo "${ts}" > ${schema_file}.$j.last
               break
            fi
            let j=$j+1
         done
         if [[ "${match}" = "N" ]]; then
            echo
            echo "Writing schema version: $v starting at record: $i."
            echo -n "Reading record "
            #echo "${def}" | jq '.' > ${schema_file}.$v
            echo "${def}" | jq -S '.' > ${schema_file}.$v
            echo "${ts}" > ${schema_file}.$v.first
            echo "${ts}" > ${schema_file}.$v.last
            let v=$v+1
         fi
      else
         echo
         echo "Writing schema version: $v starting at record: $i."
         echo -n "Reading record "
         #echo "${def}" | jq '.' > ${schema_file}.$v
         echo "${def}" | jq -S '.' > ${schema_file}.$v
         echo "${ts}" > ${schema_file}.$v.first
         echo "${ts}" > ${schema_file}.$v.last
         let v=$v+1
      fi
      let i=$i+1
      let n=$n+1
   done
done<${sample_file}
echo
echo "Done."
exit
