#!/bin/bash

if [[ "$(whoami)" != "root" ]]; then
   echo "Script must be run as the root user.  Exiting."
   exit
fi
export country="US"
export state="VA"
export city="Charlottesville"
export org="Zenopz"
export admin_email="justin.gluchowski@zenethtechpartners.com"
export hostname=$(hostname)
export fqdn=$(hostname -f)
export role=${hostname%vpn}
cadir="${HOME}/vpn-ca"
privdir="${cadir}/keys"
clientdir="${cadir}/client-configs"
exists=$(grep "###$1###" ${clientdir}/client.list)

if [ -z $1 ]; then
   echo "Must pass in the name of the client.  Exiting."
   exit
elif [[ "${exists}" != "" ]]; then
   echo "It appears that the client name provided already has a profile.  Exiting."
   exit
fi
client=$1
mkdir ${clientdir}/${client}

if [ ! -z $2 ]; then
   pw="--password $2"
   echo "$2" > "${clientdir}/${client}/${client}-${role}.psk"
fi

cd "${cadir}"
source vars >/dev/null 2>&1
export KEY_CN="CN=${client},OU=${role},O=${org}"
export KEY_NAME="${client}-${role}-${org}"
./pkitool --interact ${pw} ${client}
cat "${clientdir}/base.conf" \
   <(echo -e '<ca>') \
   "${privdir}/ca.crt" \
   <(echo -e '</ca>\n<cert>') \
   "${privdir}/${client}.crt" \
   <(echo -e '</cert>\n<key>') \
   "${privdir}/${client}.key" \
   <(echo -e '</key>\n<tls-auth>') \
   "${privdir}/ta.key" \
   <(echo -e '</tls-auth>') \
> "${clientdir}/${client}/${client}-${role}.ovpn"
cd ${clientdir}
tar cfz "${client}-${role}.tgz" ${client}
rm -rf ${client}
echo "###${client}###" >> "${clientdir}/client.list"
echo "Done.  File: ${clientdir}/${client}-${role}.tgz."

