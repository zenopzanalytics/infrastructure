#!/bin/bash

if [[ "$(whoami)" != "root" ]]; then
   echo "Script must be run as the root user.  Exiting."
   exit
fi

this=$0 && echo "${this}"
this_file=$(basename ${this}) && echo "${this_file}"
this_path="${this%/${this_file}}" && echo "${this_path}"
env_file="${this_path}/${this_file%sh}env" && echo "${env_file}"

if [[ -r "${env_file}" ]]; then
   source "${env_file}"
else
   echo "Missing environment script (${env_file}).  Exiting."
   exit
fi

cadir="${HOME}/vpn-ca"

apt-get -qq update
apt-get -qq -y install openvpn easy-rsa

make-cadir "${cadir}"
cd "${cadir}"
mv vars vars.orig
cat vars.orig | \
   sed -e 's/KEY_COUNTRY=".*"/KEY_COUNTRY="${country}"/' | \
   sed -e 's/KEY_PROVINCE=".*"/KEY_PROVINCE="${state}"/' | \
   sed -e 's/KEY_CITY=".*"/KEY_CITY="${city}"/' | \
   sed -e 's/KEY_ORG=".*"/KEY_ORG="${org}"/' | \
   sed -e 's/KEY_EMAIL=".*"/KEY_EMAIL="${admin_email}"/' | \
   sed -e 's/KEY_OU=".*"/KEY_OU="${role}"/' \
> vars
source vars >/dev/null 2>&1
./clean-all >/dev/null 2>&1
echo -n "Building CA certs and keys..."
mv pkitool pkitool.orig
cat pkitool.orig | \
   sed -e '/--pass     ) NODES_REQ="" ;;/a --password ) NODES_REQ="-passout pass:$2"; shift;;' \
> pkitool
chmod 777 pkitool
export KEY_NAME="Zenopz VPN CA - ${role}"
./pkitool --batch --initca >/dev/null 2>&1
echo " Done."
export KEY_CN="${fqdn}"
export KEY_NAME="${fqdn}"
echo "Interactive generation of certs and keys for ${KEY_CN}."
./pkitool --interact --server "${fqdn}"
echo -n "Generating DH parameters.  Please be patient... "
./build-dh >/dev/null 2>&1
echo "Done."
openvpn --genkey --secret keys/ta.key
for item in "ca.crt:444" "${fqdn}.crt:444" "${fqdn}.key:400" "ta.key:400" "dh2048.pem:444"; do
   file=$(echo "${item}" | cut -d':' -f1)
   perm=$(echo "${item}" | cut -d':' -f2)
   cp keys/"${file}" /etc/openvpn && chmod "${perm}" /etc/openvpn/"${file}"
done
sample="/usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz"
conf_file="/etc/openvpn/${fqdn}.conf"
gunzip -c "${sample}" | \
   sed -e "s/cert server\.crt/cert $(echo ${fqdn} | sed -e 's/\./\\\./g')\.crt/" | \
   sed -e "s/key server\.key/key $(echo ${fqdn} | sed -e 's/\./\\\./g')\.key/" | \
   sed -e "s/server 10\.8\.0\.0 255\.255\.255\.0/server $(echo ${vpn_net_addr} ${vpn_net_mask} | sed -e 's/\./\\\./g')/" | \
   sed -e "s/comp-lzo/#comp-lzo/" | \
   sed -e "s/;user/user/" | \
   sed -e "s/;group/group/" | \
   sed -e "s/;tls-auth/tls-auth/" | \
   sed -e "/tls-auth ta\.key 0/a key-direction 0" | \
   sed -e "s/;cipher AES-128-CBC/cipher AES-128-CBC/" | \
   sed -e "/cipher AES-128-CBC/a auth SHA256" | \
   sed -e "s/;topology/topology/" \
> "${conf_file}"
echo "push \"route ${vnet_addr} ${vnet_mask}\"" >> "${conf_file}"
echo "push \"dhcp-option DNS 10.11.12.5\"" >> "${conf_file}"
echo "push \"dhcp-option DNS 8.8.8.8\"" >> "${conf_file}"
echo "push \"dhcp-option DNS 8.8.4.4\"" >> "${conf_file}"
echo "push \"dhcp-option DOMAIN-SEARCH ${domain}\"" >> "${conf_file}"
mkdir client-configs
chmod 700 client-configs
client_sample="/usr/share/doc/openvpn/examples/sample-config-files/client.conf"
base_conf="client-configs/base.conf"
cat "${client_sample}" | \
   sed -e "s/remote my-server-1/remote ${pubvpnname}/" | \
   sed -e "s/ca ca.crt/#ca ca.crt/" | \
   sed -e "s/cert client.crt/#cert client.crt/" | \
   sed -e "s/key client.key/#key client.key/" | \
   sed -e "s/comp-lzo/#comp-lzo/" | \
   sed -e "s/;user/user/" | \
   sed -e "s/;group/group/" | \
   sed -e "/;cipher x/a auth SHA256" | \
   sed -e "/;cipher x/a cipher AES-128-CBC" | \
   sed -e "/;tls-auth ta\.key 1/a key-direction 1" \
> "${base_conf}"
echo "Done."

if [ ! -f /etc/sysctl.conf.orig ]; then
   cp -p /etc/sysctl.conf /etc/sysctl.conf.orig
fi
cat /etc/sysctl.conf.orig | \
   sed -e "s/#net\.ipv4\.ip_forward=1/net\.ipv4\.ip_forward=1/" \
> /etc/sysctl.conf
sysctl -p

systemctl start openvpn@"${fqdn}"
systemctl enable openvpn@"${fqdn}"
