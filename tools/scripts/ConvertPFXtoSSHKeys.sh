#!/bin/bash

if [ -z $1 ]; then
   echo "Must provide the fully qualified file name for the certificate (.pfx)."
   exit
fi
fullname="$1"
filename="$(basename ${fullname})"
filepath="${fullname%${filename}}"
subject="${filename%.pfx}"
if [[ ${#2} -gt 0 ]]; then
   pw=$2
fi
privfile="${filepath}${subject}_rsa"
pubfile="${privfile}.pub"
tempfile="${privfile}.tmp"
echo "Filename: ${filename}.  Filepath: ${filepath}.  Subject: ${subject}."
echo -n "Generating SSH public/private key pair..."
openssl pkcs12 -in "${fullname}" -nocerts -nodes -passin "pass:${pw}" 2>/dev/null | openssl rsa -out "${privfile}" >/dev/null 2>&1
openssl rsa -in "${privfile}" -pubout -RSAPublicKey_out -out "${tempfile}" >/dev/null 2>&1
ssh-keygen -f "${tempfile}" -i -m pem > "${pubfile}" 2>/dev/null
rm -rf "${tempfile}"
echo "Done."
echo "Private Key: ${privfile}.  Public Key: ${pubfile}."

