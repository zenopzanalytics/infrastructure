#!/bin/bash
exec 3>&1 && exec >&2
let len=128
if [ ! -z $1 ]; then
   let len=$1
fi
echo "Generating a random $len character value to be used as a shared key..."
let maxind=$len-1
keyval=$(openssl rand $len -base64 | tr -d '\n' | cut -c-$maxind)
outlen=$(echo ${keyval} | wc -c)
echo "Key of length ${outlen} generated:"
echo "${keyval}" >&3 3>&-

