#!/bin/bash

if [ -z $1 ]; then
   echo "Must provide DN (e.g. CN=jdoe,OU=staging,O=zenopz)."
   exit
fi
dn=$1
if [[ ${#2} -ge 5 ]]; then
   pw=$2
elif [[ ${#2} -gt 0 ]]; then
   echo "Provided password must have at least 5 characters.  Not using."
fi
OIFS=$IFS && IFS=$","
dnparts=($1)
IFS=$OIFS
for i in ${!dnparts[@]}; do
   IFS=$"="
   kvp=(${dnparts[$i]})
   IFS=$OIFS
   case "${kvp[0]}" in
      cn|CN) cn="${kvp[1]}";;
      ou|OU) ou="${kvp[1]}";;
      o|O)   o="${kvp[1]}";;
   esac
done
outdir="keys/${o}/${ou}" && mkdir -p "${outdir}"
prikey="${outdir}/${cn}-${ou}_rsa"
pubkey="${prikey}.pub"
pwfile="${prikey}.pass"
echo "Generating SSH keys for ${cn} for the ${ou} environment..."
ssh-keygen -t rsa -b 4096 -C "${dn}" -f "${prikey}" -N "${pw}" -q
if [[ "${pw}" = "" ]]; then
   echo "Done.  Private Key: ${prikey}.  Public Key: ${pubkey}."
else
   echo "${pw}" > "${pwfile}"
   echo "Done.  Private Key: ${prikey}.  Public Key: ${pubkey}.  Pass: ${pwfile}."
fi
